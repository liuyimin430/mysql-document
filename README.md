# mysql-document

**主要演示给 MySQL 数据库表在线生成设计文档** (还支持其他数据库)

支持三种文档类型导出
- HTML
- Word
- Markdown

演示得文档导出放在 /resources/documnet/ 下