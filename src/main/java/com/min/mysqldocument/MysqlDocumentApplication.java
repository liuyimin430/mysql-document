package com.min.mysqldocument;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MysqlDocumentApplication {

    public static void main(String[] args) {
        SpringApplication.run(MysqlDocumentApplication.class, args);
    }

}
