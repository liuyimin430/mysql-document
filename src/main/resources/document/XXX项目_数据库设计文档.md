# 数据库设计文档

**数据库名：** smart_bolt

**文档版本：** 2022.10.29

**文档描述：** XXX项目数据库设计文档

| 表名                  | 说明       |
| :---: | :---: |
| [bolt_accept](#bolt_accept) | 数据接收表 |
| [bolt_alarm](#bolt_alarm) | 告警信息表 |
| [bolt_alarm_copy1](#bolt_alarm_copy1) | 告警信息表 |
| [bolt_alarm_information](#bolt_alarm_information) | 告警信息记录表 |
| [bolt_bolt](#bolt_bolt) | 螺栓设备表 |
| [bolt_fiber_sensor](#bolt_fiber_sensor) | 光纤式传感器设备表 |
| [bolt_fiber_sensor_data](#bolt_fiber_sensor_data) | 光纤式传感器数据接收表 |
| [bolt_loose_sensor](#bolt_loose_sensor) | 松动式传感器设备表 |
| [bolt_loose_sensor_data](#bolt_loose_sensor_data) | 松动式传感器数据接收表 |
| [bolt_loose_threshold](#bolt_loose_threshold) | 松动传感器设备表 |
| [bolt_loose_transducer](#bolt_loose_transducer) | 松动传感器数据接收表 |
| [bolt_transducer](#bolt_transducer) | 传感器设备表 |
| [wh_attendance_rule](#wh_attendance_rule) | 考勤规则 |
| [wh_pharmacist_attendance](#wh_pharmacist_attendance) | 药师考勤 |

**表名：** <a id="bolt_accept">bolt_accept</a>

**说明：** 数据接收表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | 主键id  |
|  2   | transducer_name |   varchar   | 255 |   0    |    Y     |  N   |       | 传感器名称  |
|  3   | transducer_code |   varchar   | 100 |   0    |    Y     |  N   |       | 传感器编号  |
|  4   | relative_zero |   varchar   | 100 |   0    |    Y     |  N   |       | 相对零点  |
|  5   | real_value |   varchar   | 100 |   0    |    Y     |  N   |       | 实时值  |
|  6   | accept_data |   varchar   | 200 |   0    |    Y     |  N   |       | 接收的原始数据  |
|  7   | accept_time |   datetime   | 19 |   0    |    Y     |  N   |       | 接收时间  |
|  8   | create_user |   bigint   | 20 |   0    |    Y     |  N   |       | 创建人  |
|  9   | create_dept |   bigint   | 20 |   0    |    Y     |  N   |       | 创建部门  |
|  10   | create_time |   datetime   | 19 |   0    |    Y     |  N   |       | 创建时间  |
|  11   | update_user |   bigint   | 20 |   0    |    Y     |  N   |       | 修改人  |
|  12   | update_time |   datetime   | 19 |   0    |    Y     |  N   |       | 修改时间  |
|  13   | status |   int   | 10 |   0    |    Y     |  N   |       | 状态  |
|  14   | is_deleted |   int   | 10 |   0    |    Y     |  N   |       | 是否已删除  |

**表名：** <a id="bolt_alarm">bolt_alarm</a>

**说明：** 告警信息表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | 主键id  |
|  2   | bolt_code |   varchar   | 100 |   0    |    Y     |  N   |       | 螺栓编号  |
|  3   | transducer_code |   varchar   | 100 |   0    |    Y     |  N   |       | 传感器编号  |
|  4   | transducer_type |   varchar   | 100 |   0    |    Y     |  N   |       | 传感器类型(1-轴力;2-剪力;3-温补;4-松动;5-压力)  |
|  5   | alarm_type |   int   | 10 |   0    |    Y     |  N   |       | 告警类型(1-低于阈值下限;2-高于阈值上限;3-传感器离线告警;4-螺栓离线告警;5-螺栓松动告警)  |
|  6   | alarm_content |   varchar   | 255 |   0    |    Y     |  N   |       | 告警内容  |
|  7   | create_user |   bigint   | 20 |   0    |    Y     |  N   |       | 创建人  |
|  8   | create_dept |   bigint   | 20 |   0    |    Y     |  N   |       | 创建部门  |
|  9   | create_time |   datetime   | 19 |   0    |    Y     |  N   |       | 创建时间  |
|  10   | update_user |   bigint   | 20 |   0    |    Y     |  N   |       | 修改人  |
|  11   | update_time |   datetime   | 19 |   0    |    Y     |  N   |       | 修改时间  |
|  12   | status |   int   | 10 |   0    |    Y     |  N   |       | 状态  |
|  13   | is_deleted |   int   | 10 |   0    |    Y     |  N   |       | 是否已删除  |

**表名：** <a id="bolt_alarm_copy1">bolt_alarm_copy1</a>

**说明：** 告警信息表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | 主键id  |
|  2   | bolt_code |   varchar   | 100 |   0    |    Y     |  N   |       | 螺栓编号  |
|  3   | transducer_code |   varchar   | 100 |   0    |    Y     |  N   |       | 传感器编号  |
|  4   | transducer_type |   varchar   | 100 |   0    |    Y     |  N   |       | 传感器类型(1-轴力;2-剪力;3-温补;4-松动;5-压力)  |
|  5   | alarm_type |   int   | 10 |   0    |    Y     |  N   |       | 告警类型(1-低于阈值下限;2-高于阈值上限;3-传感器离线告警;4-螺栓离线告警;5-螺栓松动告警)  |
|  6   | alarm_content |   varchar   | 255 |   0    |    Y     |  N   |       | 告警内容  |
|  7   | create_user |   bigint   | 20 |   0    |    Y     |  N   |       | 创建人  |
|  8   | create_dept |   bigint   | 20 |   0    |    Y     |  N   |       | 创建部门  |
|  9   | create_time |   datetime   | 19 |   0    |    Y     |  N   |       | 创建时间  |
|  10   | update_user |   bigint   | 20 |   0    |    Y     |  N   |       | 修改人  |
|  11   | update_time |   datetime   | 19 |   0    |    Y     |  N   |       | 修改时间  |
|  12   | status |   int   | 10 |   0    |    Y     |  N   |       | 状态  |
|  13   | is_deleted |   int   | 10 |   0    |    Y     |  N   |       | 是否已删除  |

**表名：** <a id="bolt_alarm_information">bolt_alarm_information</a>

**说明：** 告警信息记录表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | 主键id  |
|  2   | create_user |   bigint   | 20 |   0    |    Y     |  N   |       | 创建人  |
|  3   | create_dept |   bigint   | 20 |   0    |    Y     |  N   |       | 创建部门  |
|  4   | create_time |   datetime   | 19 |   0    |    Y     |  N   |       | 创建时间  |
|  5   | update_user |   bigint   | 20 |   0    |    Y     |  N   |       | 修改人  |
|  6   | update_time |   datetime   | 19 |   0    |    Y     |  N   |       | 修改时间  |
|  7   | status |   int   | 10 |   0    |    Y     |  N   |       | 状态  |
|  8   | is_deleted |   int   | 10 |   0    |    Y     |  N   |       | 是否已删除  |

**表名：** <a id="bolt_bolt">bolt_bolt</a>

**说明：** 螺栓设备表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | 主键id  |
|  2   | create_user |   bigint   | 20 |   0    |    Y     |  N   |       | 创建人  |
|  3   | create_dept |   bigint   | 20 |   0    |    Y     |  N   |       | 创建部门  |
|  4   | create_time |   datetime   | 19 |   0    |    Y     |  N   |       | 创建时间  |
|  5   | update_user |   bigint   | 20 |   0    |    Y     |  N   |       | 修改人  |
|  6   | update_time |   datetime   | 19 |   0    |    Y     |  N   |       | 修改时间  |
|  7   | status |   int   | 10 |   0    |    Y     |  N   |       | 状态  |
|  8   | is_deleted |   int   | 10 |   0    |    Y     |  N   |       | 是否已删除  |
|  9   | bolt_code |   varchar   | 50 |   0    |    Y     |  N   |       | 螺栓编号  |
|  10   | bolt_name |   varchar   | 50 |   0    |    Y     |  N   |       | 螺栓名称  |
|  11   | location |   varchar   | 100 |   0    |    Y     |  N   |       | 安装位置  |
|  12   | last_communication_time |   datetime   | 19 |   0    |    Y     |  N   |       | 上一次通信时间  |
|  13   | is_online |   int   | 10 |   0    |    Y     |  N   |       | 是否在线(0-离线状态;1-在线状态)  |

**表名：** <a id="bolt_fiber_sensor">bolt_fiber_sensor</a>

**说明：** 光纤式传感器设备表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | 主键id  |
|  2   | create_user |   bigint   | 20 |   0    |    Y     |  N   |       | 创建人  |
|  3   | create_dept |   bigint   | 20 |   0    |    Y     |  N   |       | 创建部门  |
|  4   | create_time |   datetime   | 19 |   0    |    Y     |  N   |       | 创建时间  |
|  5   | update_user |   bigint   | 20 |   0    |    Y     |  N   |       | 修改人  |
|  6   | update_time |   datetime   | 19 |   0    |    Y     |  N   |       | 修改时间  |
|  7   | status |   int   | 10 |   0    |    Y     |  N   |       | 状态  |
|  8   | is_deleted |   int   | 10 |   0    |    Y     |  N   |       | 是否已删除  |

**表名：** <a id="bolt_fiber_sensor_data">bolt_fiber_sensor_data</a>

**说明：** 光纤式传感器数据接收表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | 主键id  |
|  2   | create_user |   bigint   | 20 |   0    |    Y     |  N   |       | 创建人  |
|  3   | create_dept |   bigint   | 20 |   0    |    Y     |  N   |       | 创建部门  |
|  4   | create_time |   datetime   | 19 |   0    |    Y     |  N   |       | 创建时间  |
|  5   | update_user |   bigint   | 20 |   0    |    Y     |  N   |       | 修改人  |
|  6   | update_time |   datetime   | 19 |   0    |    Y     |  N   |       | 修改时间  |
|  7   | status |   int   | 10 |   0    |    Y     |  N   |       | 状态  |
|  8   | is_deleted |   int   | 10 |   0    |    Y     |  N   |       | 是否已删除  |

**表名：** <a id="bolt_loose_sensor">bolt_loose_sensor</a>

**说明：** 松动式传感器设备表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | 主键id  |
|  2   | create_user |   bigint   | 20 |   0    |    Y     |  N   |       | 创建人  |
|  3   | create_dept |   bigint   | 20 |   0    |    Y     |  N   |       | 创建部门  |
|  4   | create_time |   datetime   | 19 |   0    |    Y     |  N   |       | 创建时间  |
|  5   | update_user |   bigint   | 20 |   0    |    Y     |  N   |       | 修改人  |
|  6   | update_time |   datetime   | 19 |   0    |    Y     |  N   |       | 修改时间  |
|  7   | status |   int   | 10 |   0    |    Y     |  N   |       | 状态  |
|  8   | is_deleted |   int   | 10 |   0    |    Y     |  N   |       | 是否已删除  |

**表名：** <a id="bolt_loose_sensor_data">bolt_loose_sensor_data</a>

**说明：** 松动式传感器数据接收表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | 主键id  |
|  2   | create_user |   bigint   | 20 |   0    |    Y     |  N   |       | 创建人  |
|  3   | create_dept |   bigint   | 20 |   0    |    Y     |  N   |       | 创建部门  |
|  4   | create_time |   datetime   | 19 |   0    |    Y     |  N   |       | 创建时间  |
|  5   | update_user |   bigint   | 20 |   0    |    Y     |  N   |       | 修改人  |
|  6   | update_time |   datetime   | 19 |   0    |    Y     |  N   |       | 修改时间  |
|  7   | status |   int   | 10 |   0    |    Y     |  N   |       | 状态  |
|  8   | is_deleted |   int   | 10 |   0    |    Y     |  N   |       | 是否已删除  |

**表名：** <a id="bolt_loose_threshold">bolt_loose_threshold</a>

**说明：** 松动传感器设备表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | 主键id  |
|  2   | bolt_code |   varchar   | 100 |   0    |    N     |  N   |       | 螺栓编号  |
|  3   | loose_transducer_code |   varchar   | 20 |   0    |    N     |  N   |       | 松动传感器编号  |
|  4   | loose_transducer_name |   varchar   | 255 |   0    |    Y     |  N   |       | 松动传感器名称  |
|  5   | xaxis_threshold_low |   double   | 11 |   2    |    Y     |  N   |       | X轴阈值下限  |
|  6   | xaxis_threshold_high |   double   | 11 |   2    |    Y     |  N   |       | X轴阈值上限  |
|  7   | yaxis_threshold_low |   double   | 11 |   2    |    Y     |  N   |       | Y轴阈值下限  |
|  8   | yaxis_threshold_high |   double   | 11 |   2    |    Y     |  N   |       | Y轴阈值上限  |
|  9   | zaxis_threshold_low |   double   | 11 |   2    |    Y     |  N   |       | Z轴阈值下限  |
|  10   | zaxis_threshold_high |   double   | 11 |   2    |    Y     |  N   |       | Z轴阈值上限  |
|  11   | last_communication_time |   datetime   | 19 |   0    |    Y     |  N   |       | 上一次通信时间  |
|  12   | is_online |   int   | 10 |   0    |    Y     |  N   |   0    | 是否在线(0-离线状态;1-在线状态)  |
|  13   | is_loose |   char   | 1 |   0    |    Y     |  N   |       | 是否松动(0-未松动;1-已松动)  |
|  14   | create_user |   bigint   | 20 |   0    |    Y     |  N   |       | 创建人  |
|  15   | create_dept |   bigint   | 20 |   0    |    Y     |  N   |       | 创建部门  |
|  16   | create_time |   datetime   | 19 |   0    |    Y     |  N   |       | 创建时间  |
|  17   | update_user |   bigint   | 20 |   0    |    Y     |  N   |       | 修改人  |
|  18   | update_time |   datetime   | 19 |   0    |    Y     |  N   |       | 修改时间  |
|  19   | status |   int   | 10 |   0    |    Y     |  N   |       | 状态  |
|  20   | is_deleted |   int   | 10 |   0    |    Y     |  N   |       | 是否已删除  |

**表名：** <a id="bolt_loose_transducer">bolt_loose_transducer</a>

**说明：** 松动传感器数据接收表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | 主键id  |
|  2   | bolt_code |   varchar   | 50 |   0    |    Y     |  N   |       | 螺栓编号  |
|  3   | loose_transducer_code |   varchar   | 50 |   0    |    Y     |  N   |       | 松动传感器编号  |
|  4   | xaxis |   double   | 11 |   2    |    Y     |  N   |       | X轴位移  |
|  5   | yaxis |   double   | 11 |   2    |    Y     |  N   |       | Y轴位移  |
|  6   | zaxis |   double   | 11 |   2    |    Y     |  N   |       | Z轴位移  |
|  7   | create_user |   bigint   | 20 |   0    |    Y     |  N   |       | 创建人  |
|  8   | create_dept |   bigint   | 20 |   0    |    Y     |  N   |       | 创建部门  |
|  9   | create_time |   datetime   | 19 |   0    |    Y     |  N   |       | 创建时间  |
|  10   | update_user |   bigint   | 20 |   0    |    Y     |  N   |       | 修改人  |
|  11   | update_time |   datetime   | 19 |   0    |    Y     |  N   |       | 修改时间  |
|  12   | status |   int   | 10 |   0    |    Y     |  N   |       | 状态  |
|  13   | is_deleted |   int   | 10 |   0    |    Y     |  N   |       | 是否已删除  |

**表名：** <a id="bolt_transducer">bolt_transducer</a>

**说明：** 传感器设备表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | 主键id  |
|  2   | create_user |   bigint   | 20 |   0    |    Y     |  N   |       | 创建人  |
|  3   | create_dept |   bigint   | 20 |   0    |    Y     |  N   |       | 创建部门  |
|  4   | create_time |   datetime   | 19 |   0    |    Y     |  N   |       | 创建时间  |
|  5   | update_user |   bigint   | 20 |   0    |    Y     |  N   |       | 修改人  |
|  6   | update_time |   datetime   | 19 |   0    |    Y     |  N   |       | 修改时间  |
|  7   | status |   int   | 10 |   0    |    Y     |  N   |       | 状态  |
|  8   | is_deleted |   int   | 10 |   0    |    Y     |  N   |       | 是否已删除  |
|  9   | bolt_code |   varchar   | 255 |   0    |    Y     |  N   |       | 螺栓编号  |
|  10   | transducer_code |   varchar   | 255 |   0    |    Y     |  N   |       | 传感器编号  |
|  11   | transducer_name |   varchar   | 255 |   0    |    Y     |  N   |       | 传感器名称  |
|  12   | transducer_type |   int   | 10 |   0    |    Y     |  N   |       | 传感器类型(1-轴力;2-温度)  |
|  13   | unit |   varchar   | 10 |   0    |    Y     |  N   |       | 单位  |
|  14   | lower_threshold |   varchar   | 10 |   0    |    Y     |  N   |       | 下限阈值  |
|  15   | current_value |   varchar   | 10 |   0    |    Y     |  N   |       | 最新值  |
|  16   | upper_threshold |   varchar   | 10 |   0    |    Y     |  N   |       | 上限阈值  |
|  17   | base_value |   varchar   | 10 |   0    |    Y     |  N   |       | 基准值  |
|  18   | last_communication_time |   datetime   | 19 |   0    |    Y     |  N   |       | 上一次通信时间  |
|  19   | is_online |   int   | 10 |   0    |    Y     |  N   |       | 是否在线(0-离线状态;1-在线状态)  |
|  20   | alarm_type |   varchar   | 255 |   0    |    Y     |  N   |       |   |
|  21   | alarm_time |   datetime   | 19 |   0    |    Y     |  N   |       |   |

**表名：** <a id="wh_attendance_rule">wh_attendance_rule</a>

**说明：** 考勤规则

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | 主键id  |
|  2   | create_user |   bigint   | 20 |   0    |    Y     |  N   |       | 创建人  |
|  3   | create_dept |   bigint   | 20 |   0    |    Y     |  N   |       | 创建部门  |
|  4   | create_time |   datetime   | 19 |   0    |    Y     |  N   |       | 创建时间  |
|  5   | update_user |   bigint   | 20 |   0    |    Y     |  N   |       | 修改人  |
|  6   | update_time |   datetime   | 19 |   0    |    Y     |  N   |       | 修改时间  |
|  7   | status |   int   | 10 |   0    |    Y     |  N   |       | 状态  |
|  8   | is_deleted |   int   | 10 |   0    |    Y     |  N   |       | 是否已删除  |
|  9   | am_clock_time_start |   datetime   | 19 |   0    |    Y     |  N   |       | 上午打卡时间开始  |
|  10   | am_clock_time_end |   datetime   | 19 |   0    |    Y     |  N   |       | 上午打卡时间结束  |
|  11   | pm_clock_time_start |   datetime   | 19 |   0    |    Y     |  N   |       | 下午打卡时间开始  |
|  12   | pm_clock_time_end |   datetime   | 19 |   0    |    Y     |  N   |       | 下午打卡时间结束  |
|  13   | pharmacy_ids |   varchar   | 500 |   0    |    Y     |  N   |       | 药店ids  |

**表名：** <a id="wh_pharmacist_attendance">wh_pharmacist_attendance</a>

**说明：** 药师考勤

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | 主键id  |
|  2   | create_user |   bigint   | 20 |   0    |    Y     |  N   |       | 创建人  |
|  3   | create_dept |   bigint   | 20 |   0    |    Y     |  N   |       | 创建部门  |
|  4   | create_time |   datetime   | 19 |   0    |    Y     |  N   |       | 创建时间  |
|  5   | update_user |   bigint   | 20 |   0    |    Y     |  N   |       | 修改人  |
|  6   | update_time |   datetime   | 19 |   0    |    Y     |  N   |       | 修改时间  |
|  7   | status |   int   | 10 |   0    |    Y     |  N   |       | 状态  |
|  8   | is_deleted |   int   | 10 |   0    |    Y     |  N   |       | 是否已删除  |
|  9   | name |   varchar   | 100 |   0    |    Y     |  N   |       | 考勤名称  |
|  10   | pharmacist_id |   bigint   | 20 |   0    |    N     |  N   |       | 药师id  |
|  11   | user_id |   bigint   | 20 |   0    |    N     |  N   |       | 药师id  |
|  12   | am_clock_time |   datetime   | 19 |   0    |    Y     |  N   |       | 上午打卡时间  |
|  13   | pm_clock_time |   datetime   | 19 |   0    |    Y     |  N   |       | 下午打卡时间  |
|  14   | interval_time |   varchar   | 50 |   0    |    Y     |  N   |       | 间隔时间(分钟)  |
|  15   | is_arrive_late |   int   | 10 |   0    |    Y     |  N   |       | 是否迟到(0:否,1:是)  |
|  16   | is_leave_early |   int   | 10 |   0    |    Y     |  N   |       | 是否早退(0:否,1:是)  |
